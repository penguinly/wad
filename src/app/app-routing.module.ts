import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';  

import { HomeComponent } from "./home/home.component"; 
import { FriendComponent } from "./friend/friend.component";  
import { HeaderComponent } from "./header/header.component"; 
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";                                           
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FriendDetailComponent } from './friend-detail/friend-detail.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'friend', component: FriendComponent},
  {path: 'contact-us', component: ContactUsComponent},
  {path: 'not-found', component: PageNotFoundComponent},
  {path: 'friend-detail/:fr_id', component: FriendDetailComponent},
  {path: '**', redirectTo: '/not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
