import { Component } from '@angular/core';
import { FriendsService } from './friend/friends.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [FriendsService]
})
export class AppComponent {
  title = 'MyFriends App';
}
