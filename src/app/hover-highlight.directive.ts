import { Directive, 
  ElementRef, 
  Renderer2, 
  OnInit, 
  HostListener, 
  HostBinding, 
  Input
 } from '@angular/core';
import { NgModuleResolver } from '@angular/compiler';

@Directive({
  selector: '[appHoverHighlight]'
})
export class HoverHighlightDirective {

@Input('appHoverHighlight') highlightColor: {
                                              background: string,
                                              text: string
                                            };

  @HostBinding('style.color') textColor: string;
  @HostBinding('style.textDecoration') textDecoration: string;

  constructor(private elRef: ElementRef,
              private renderer: Renderer2) { }



              @HostListener('mouseenter') mouseOver(eventData: Event){
                this.renderer.setStyle(this.elRef.nativeElement, 'background-color',
                                      this.highlightColor.background);
                // this.renderer.setStyle(this.elRef.nativeElement, 'color', 'red')
                this.textColor = this.highlightColor.text;
                this.textDecoration = 'underline';
              }

              @HostListener('mouseleave') mouseExit(eventData: Event){
                this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'transparent')
                // this.renderer.setStyle(this.elRef.nativeElement, 'color', 'black')
                this.textColor = 'black';
                this.textDecoration = 'none';
              }

              // @HostListener('keypress') keyPress(eventData: Event){
              //   this.renderer.setStyle(this.elRef.nativeElement, 'color', 'blue')
              // }

              // @HostListener('window:keypress') windowKeyPress(eventData: Event){
              //   this.renderer.setStyle(this.elRef.nativeElement, 'color', 'green')
              // }




        // ngOnInit() {
        //   this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'grey');
        // }

}
