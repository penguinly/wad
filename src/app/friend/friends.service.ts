import { Injectable, EventEmitter } from '@angular/core';
import { Friend } from "./friend.model";

@Injectable({
  providedIn: 'root'
})
export class FriendsService {
  friendAdded = new EventEmitter<void>();

  private friendList : Friend [] = [
    new Friend(0, 'Peter Ho', 'pete@gmail.com', '912345678', 'Dover'),
    new Friend(1, 'Da Ming', 'DM@gmail.com', '987654321', 'Clementi'),
    new Friend (2, 'Raphbear69', 'RapItRaph@gmail.com', '97682418', 'Bukit Batok')
  ];

  friendDeleted = new EventEmitter<void>();

  constructor() { }

  addFriend(newFriendInfo){
    this.friendList.push(newFriendInfo)
    console.log("Inside Friends Service");
    console.log(this.friendList);
    this.friendAdded.emit();
  }

  getFriends() {
    return this.friendList.slice();
  }


  getFriend(fr_id){
    for (let friend of this.friendList) {
      if (friend.fr_id == fr_id){
        return friend;
      }
    }
    return undefined;
    // return this.friendList.slice();
  }

  deleteFriend(index){
    this.friendList.splice(index, 1);
    this.friendDeleted.emit();
  };











}
