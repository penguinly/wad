import { Component, OnInit, Input } from '@angular/core';
import { Friend } from '../friend.model';
import { FriendsService } from '../friends.service';
import { Router } from "@angular/router";


@Component({
  selector: 'app-friend-list',
  templateUrl: './friend-list.component.html',
  styleUrls: ['./friend-list.component.css']
})
export class FriendListComponent implements OnInit {

  @Input() myFriends: Friend[];

  sgHeros = ['GGeremy', 'JoshGayLord', 'RapeitRaph']

  constructor(private friendsService: FriendsService,
              private router: Router
              ) { }

  ngOnInit() {
  }

  onDeleteFriend(index){
    this.friendsService.deleteFriend(index);
  }

  onViewMore(fr_id){
    this.router.navigate(['friend-detail', fr_id])
  }

}
