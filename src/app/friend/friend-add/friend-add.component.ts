import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { Friend } from "../friend.model";
// import { $ } from 'protractor';
import { FriendsService } from "../friends.service";

@Component({
  selector: 'app-friend-add',
  templateUrl: './friend-add.component.html',
  styleUrls: ['./friend-add.component.css']
})
export class FriendAddComponent implements OnInit {


  @Output() friendAdded= new EventEmitter<Friend>();

  constructor(private friendsService: FriendsService) { }

  ngOnInit() {
  }

  onAddfriend( inputName: HTMLInputElement, inputEmail: HTMLInputElement, inputContact: HTMLInputElement, inputAddress: HTMLInputElement) {
    console.log("Add Friend");
    // console.log(this.inputInfo);
    // console.log(this.inputContact.nativeElement.value);
      // console.log(`Name : ${inputName.value}`);
      // console.log(`Email : ${inputEmail.value}`); 
      // console.log(`Contact: ${inputContact.value}`);
      // console.log(`Address: ${inputAddress.value}`);
      // if (inputName.value, inputEmail.value, inputContact.value, inputAddress.value != ""){
      // this.friendAdded.emit(new Friend(
      //   inputName.value,
      //   inputEmail.value,
      //   inputContact.value,
      //   inputAddress.value
      // ));

      // inputName.value = "";
      // inputContact.value = "";
      // inputEmail.value = "";
      // }

      this.friendsService.addFriend(new Friend(
        this.friendsService.getFriends().length,
        inputName.value,
        inputEmail.value,
        inputContact.value,
        inputAddress.value
      ));
  }
}
