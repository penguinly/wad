import { Component, OnInit } from '@angular/core';
import { Friend } from "./friend.model";
import { FriendListComponent } from './friend-list/friend-list.component';
import { FriendsService } from "./friends.service";

@Component({
  selector: 'app-friend',
  templateUrl: './friend.component.html',
  styleUrls: ['./friend.component.css'],
  providers: []
})
export class FriendComponent implements OnInit {

  friendList : Friend [] = [];

  // friendList : Friend [] = [
  //   new Friend('Peter Ho', 'pete@gmail.com', '912345678', 'Dover'),
  //   new Friend('Da Ming', 'DM@gmail.com', '987654321', 'Clementi'),
  //   new Friend ('Raphbear69', 'RapItRaph@gmail.com', '97682418', 'Bukit Batok')
  // ];

  constructor(private friendsService: FriendsService) { }

  ngOnInit() {
    this.friendList = this.friendsService.getFriends();

    this.friendsService.friendAdded.subscribe( () => {
      //() => {} is an anonymous function
      this.friendList = this.friendsService.getFriends();
    });
  }



  // onFriendAdded(newFriendInfo){
  //   console.log("IN Friend Component, add a friend");
  //   console.log(this.friendList);
  //   this.friendList.push(newFriendInfo);
  // }

}
