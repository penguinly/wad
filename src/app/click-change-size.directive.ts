import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appClickChangeSize]'
})
export class ClickChangeSizeDirective {
  @HostBinding('style.fontSize') textSize:string;
  @Input('appClickChangeSize') newSize:string;

// style.fontSize is the property, textsize = binded to the html element, 
// newSize = rebinds the input to the element

@HostListener('click') mouseClick(eventData: Event){
  this.textSize = this.newSize;
}


  constructor() { }




}
