import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  constructor() { }

  contactForm: FormGroup;
  submitted = false;

  acceptedPhoneCharacters = ['0','1','2','3','4','5','6','7','8','9','(',')','+','-',' '] ;

  emailBlackList = ['test@test.com', 'temp@temp.com'];

  ngOnInit() { 
    this.contactForm = new FormGroup({
      'username' : new FormControl(null, [Validators.required, Validators.minLength(3),this.blankSpaces]),
      'useremail': new FormControl(null, [Validators.required, Validators.email, this.inEmailBlackList.bind(this)]),
      // .bind(this) is to bind the array to the this.acceptedPhoneCharacters, this will allow it to be read
      'usernumber' : new FormControl(null, [Validators.required, this.phoneCharacters.bind(this)]),
      'usermessage': new FormControl(null, [Validators.required])
    });
  }

  // key = string, object = boolean
  blankSpaces(control: FormControl): {[s:string]: boolean}  {
    if (control.value != null && control.value.trim().length === 0) {
      return {'blankSpaces' : true};
    }
    // Catch the 'if', and return nothing = everything is fine
    return null;
  }

  phoneCharacters(control: FormControl) : {[s:string]: boolean} {
    for (let char of String(control.value)) {
      // checks the list of acceptedPhoneCharacters, == -1 is to indicate it is not found
      if (this.acceptedPhoneCharacters.indexOf(char) == -1) {
        return {'phoneCharacters' : true}
      }
    }
    return null;
  }

  inEmailBlackList(control: FormControl): {[s: string]: boolean} {
    //  == -1 is to indicate it is found
    if (this.emailBlackList.indexOf(control.value) !== -1) {
      return {'emailBlackListed': true};
    }
    return null;
  }

  onSubmit() {
    console.log(this.contactForm);
    this.submitted = true;  
  }

  onBackClick(){
    this.submitted = false;
    this.contactForm.reset();
  }


}
