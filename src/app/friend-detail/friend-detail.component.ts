import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FriendsService } from '../friend/friends.service';
import { Friend } from '../friend/friend.model';

@Component({
  selector: 'app-friend-detail',
  templateUrl: './friend-detail.component.html',
  styleUrls: ['./friend-detail.component.css']
})
export class FriendDetailComponent implements OnInit {

  thisFriend: Friend;

  constructor(private activatedRoute: ActivatedRoute, 
              private friendsService: FriendsService,
              private router: Router) { }

  ngOnInit() {
    console.log(this.activatedRoute.snapshot.params["fr_id"]);
    this.thisFriend = this.friendsService.getFriend(this.activatedRoute.snapshot.params["fr_id"]);
    if (this.thisFriend === undefined){
      this.router.navigate(['not-found'])
    }
  }

  onBackClick(){
    this.router.navigate(['friend']);
  }
}
